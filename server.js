const settings = require('./server.config');

const http = require('http');

const routing = require('./server/routing');
const setUpSockets = require('./server/sockets');

const app = routing();
const server = http.createServer(app);

setUpSockets(server);

server.listen(settings.port, function () {
  console.log('server listening on port ' + settings.port);
});
