const express = require('express');
const settings = require(process.cwd() + '/server.config');

const routing = function () {
  const app = express();

  app.use(express.static('./dist'));
  app.use('/shared', express.static('./dist'));

  app.get('/host', (req, res) => {
    const reject = () => {
      res.setHeader('www-authenticate', 'Basic');
      res.sendStatus(401);
    };

    const authorization = req.headers.authorization;

    if (!authorization) {
      return reject();
    }

    const [username, password] = Buffer.from(
      authorization.replace('Basic ', ''),
      'base64'
    )
      .toString()
      .split(':');

    if (
      !(
        username === settings.masterUser && password === settings.masterPassword
      )
    ) {
      return reject();
    }

    res.sendFile(process.cwd() + '/controller.html');
  });

  return app;
};

module.exports = routing;
