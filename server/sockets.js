var { Server } = require('socket.io');

var setUpSockets = function (server) {
  var globalState = null;
  var participants = 0;
  var wss = new Server(server, {});
  var hostWSS = wss.of('/host');
  console.log('websocket server created');

  wss.on('connection', function (socket) {
    console.log('Client connected');
    participants++;
    wss.emit('participants', participants);

    var clientId = socket.id;
    if (globalState) {
      wss.to(clientId).emit('state', globalState);
    }

    socket.on('disconnect', function () {
      participants--;
      wss.emit('participants', participants);
      console.log('Client disconnected');
    });
  });

  hostWSS.on('connection', function (socket) {
    console.log('Host connected');

    socket.on('reset', function () {
      console.log('reset');
      wss.emit('reset');
    });
    socket.on('next', function () {
      console.log('next');
      wss.emit('next');
    });
    socket.on('previous', function () {
      console.log('previous');
      wss.emit('previous');
    });
    socket.on('click', function (touchPointX, touchPointY) {
      console.log(
        'click on x: ' + touchPointX * 100 + '% y: ' + touchPointY * 100 + '%'
      );
      wss.emit('click', touchPointX, touchPointY);
    });
    socket.on('draw', function (touchPointX, touchPointY) {
      console.log(
        'draw point at x: ' +
          touchPointX * 100 +
          '% y: ' +
          touchPointY * 100 +
          '%'
      );
      wss.emit('draw', touchPointX, touchPointY);
    });
    socket.on('stopDrawing', function () {
      console.log('stop drawing');
      wss.emit('stopDrawing');
    });
    socket.on('state', function (state) {
      globalState = state;
      wss.emit('state', state);
    });
    socket.on('disconnect', function () {
      console.log('Host disconnected');
    });
  });
};

module.exports = setUpSockets;
