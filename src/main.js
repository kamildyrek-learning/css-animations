import './css/style.css';

import Reveal from 'reveal.js';
import Zoom from 'reveal.js/plugin/zoom/zoom.esm.js';
import Notes from 'reveal.js/plugin/notes/notes.esm.js';
import Search from 'reveal.js/plugin/search/search.esm.js';
import Markdown from 'reveal.js/plugin/markdown/markdown.esm.js';
import Highlight from 'reveal.js/plugin/highlight/highlight.esm.js';

import initializeSocketsHandling from './socketsHandling/index.js';

const isShared = window.location.pathname.includes('shared');

const deck = new Reveal({
  hash: true,
  controls: !isShared,
  keyboard: !isShared,
  plugins: [Zoom, Notes, Search, Markdown, Highlight],
});
deck.initialize();

deck.on('ready', () => {
  if (isShared) initializeSocketsHandling(deck);
});
