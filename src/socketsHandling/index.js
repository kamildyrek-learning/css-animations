import { io } from 'socket.io-client';

import socketsPlayback from './socketsPlayback';
import socketsDrawing from './socketsDrawing';
import socketsParticipantsCounter from './socketsParticipantsCounter';

const initializeSocketsHandling = (deck) => {
  const host = location.origin.replace(/^http/, 'ws');
  const socket = io(host);

  socketsPlayback(socket, deck);

  socketsDrawing(socket);

  socketsParticipantsCounter(socket);
};

export default initializeSocketsHandling;
