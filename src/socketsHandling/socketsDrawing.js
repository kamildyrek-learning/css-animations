const slidesArea = document.querySelector('.slides-area');
const interactiveLayer = document.querySelector('#interactive-layer');

const translateToAbsoluteCoordinates = (x, y) => {
  const xAbsolute =
    x * slidesArea.getBoundingClientRect().width +
    slidesArea.getBoundingClientRect().left;
  const yAbsolute =
    y * slidesArea.getBoundingClientRect().height +
    slidesArea.getBoundingClientRect().top;

  return { x: xAbsolute, y: yAbsolute };
};

const socketsDrawing = (socket) => {
  socket.on('click', (touchPointX, touchPointY) => {
    const highlight = document.createElement('div');
    highlight.className = 'click-highlight';
    const absoluteCoordinates = translateToAbsoluteCoordinates(
      touchPointX,
      touchPointY
    );
    highlight.style.top = absoluteCoordinates.y + 'px';
    highlight.style.left = absoluteCoordinates.x + 'px';
    document.body.appendChild(highlight);
    setTimeout(() => {
      highlight.remove();
    }, 4000);
  });

  let drawing = false;
  let path = 'M';

  socket.on('stopDrawing', () => {
    drawing = false;
    path = 'M';
    const pathToRemove = interactiveLayer.querySelector('.active');
    if (pathToRemove) {
      pathToRemove.classList.remove('active');
      pathToRemove.classList.add('path-fadeout');
      setTimeout(() => {
        pathToRemove.remove();
      }, 4000);
    }
  });

  socket.on('draw', (touchPointX, touchPointY) => {
    path += `${touchPointX * 960} ${touchPointY * 700}, `;
    if (!drawing) {
      interactiveLayer.insertAdjacentHTML(
        'beforeend',
        `<path class="active" filter="url(#glow)" d="${path}"/>`
      );
      drawing = true;
    } else {
      interactiveLayer.lastChild.setAttribute('d', path);
    }
  });
};

export default socketsDrawing;
