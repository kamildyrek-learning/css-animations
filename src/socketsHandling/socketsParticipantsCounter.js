const socketsParticipantsCounter = (socket) => {
  const counterElement = document.querySelector('#participants');

  counterElement.classList.remove('hidden');
  counterElement.classList.add('flex');

  socket.on('participants', (participants) => {
    const counterTemplate = `${participants} participant${
      participants > 1 ? 's' : ''
    }`;
    counterElement.querySelector('.data').innerHTML = counterTemplate;
  });
};

export default socketsParticipantsCounter;
