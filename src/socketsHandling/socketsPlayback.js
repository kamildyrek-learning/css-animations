const socketsPlayback = (socket, deck) => {
  socket.on('reset', function () {
    deck.slide(0);
  });

  socket.on('next', function () {
    deck.next();
  });

  socket.on('previous', function () {
    deck.prev();
  });

  socket.on('state', (message) => {
    const state = JSON.parse(message).state;
    const currentState = deck.getState(state);

    const stateInconsistency = Object.keys(currentState).some((key) => {
      return currentState[key] !== state[key];
    });

    if (stateInconsistency) deck.setState(state);
  });

  deck.on('slidechanged', () => {
    const message = { state: deck.getState() };
    console.log(message);
    window.top.postMessage(JSON.stringify(message), '*');
  });
};

export default socketsPlayback;
